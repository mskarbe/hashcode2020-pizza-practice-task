# hashcode2020-pizza-practice-task

*my solution for problem described in [practice_problem.pdf](/practice_problem.pdf)*



Basically my idea for the algorithm was that if you have **N slices to order** and **P pizzas to choose from**, then you get the **S = sum of all the pizza slices available in the offer** (so slices of p1 + slices of p2 + … + spices of pizza P). At first you check if it is smaller/equal than N that you need.
Probably no, so then you iterate over arrays:
- in first step you have an array of [ pizza0, pizza1, pizza2, …, pizzaP ] and you ditch the last pizza => **[pizza0, pizza1, … , pizza(P-1)]**, 
    - so you take **tempSum = (S - pizzaP)**
    - then you check if **tempSum <= N**: 
        - if yes then you save the combination of pizzas and the sum of slices for that combination; 
        - if not then you again ditch the last pizza so new **tempSum = (tempSum - pizza(P-1))**, you again check if tempSum <= N, save it or again substract the last one so in this iteration slices of pizza number (P-2), and so on and so on… 
- The next iteration: you set back **tempSum = S**, you switch the initial array by pushing the pizzas: [ pizza0, pizza1, pizza2, …, pizzaP ] => **[ pizza1, pizza2, pizza3 …, pizzaP, pizza0 ]**. 
- Then you repeat with ditching the last pizza by **tempSum = tempSum - pizza0**, as now the pizza0 is the last one. You check again if **tempSum <= N** and so on and so on.

So you rotate the pizzas P times, each array you substract and compare max P times. 
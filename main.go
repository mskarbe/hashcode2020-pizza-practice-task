package main

import (
	"os"
	"bufio"
	"fmt"
	"strconv"
)

// struct that holds the array of possible pizzas to order & their points
// i.e. an int sum-of-slices and an array of pizzas that are possible to order, i.e. if sum-of-slices <= max slices
type possiblePizzaSet struct {
    Slices int
    Pizzas []int
}

func main() {
	var pizzas []int //array of different pizzas of different slices that are offered
	var possiblePizzasToOrder []possiblePizzaSet // an array of possible pizza sets
	sumOfAllTheSlicesOfAllAvailablePizzas := 0

	// open the input file
	file, err := os.Open("c_medium.in")
	if err != nil {
	  fmt.Println(err)
	  return
	}
	defer file.Close()

	//scanner stuff set to scan for separate strings (ScanWords)
	scanner := bufio.NewScanner(file)
	scanner.Split(bufio.ScanWords)
	//list for each string
	var words []string

	for scanner.Scan() {
		words = append(words, scanner.Text())
	}
	
	//first element is the MAX number of slices that we need to order
	slices, err := strconv.Atoi(words[0])
	if err != nil {
		fmt.Println(err)
	  }
	// second element is the number of the different pizzas in the offer
	numberOfPizzas, err := strconv.Atoi(words[1])
	if err != nil {
		fmt.Println(err)
	  }
	
	// all the following numbers are the slices in the different pizzas
	for i := 2; i < len(words); i++ {
		temp, err := strconv.Atoi(words[i])
		if err != nil {
			fmt.Println(err)
		  }
		pizzas = append(pizzas, temp)
		sumOfAllTheSlicesOfAllAvailablePizzas = sumOfAllTheSlicesOfAllAvailablePizzas + temp
	}
	// okay, ufff, finally we have all the values from the file as the vars, great
	// if the pizzas they have are not enough than sorri no sorri, but returni
	if (sumOfAllTheSlicesOfAllAvailablePizzas < slices) {
		pizzasThatAreActualResult := possiblePizzaSet{slices, pizzas}
		writeResultToFile(pizzasThatAreActualResult)
		return
	}
	
	// if not then we iterateeee
	temp := 0
	for i := 0; i < numberOfPizzas; i++ {
		tempSum := sumOfAllTheSlicesOfAllAvailablePizzas
		//we move the pieces by one, first one gets last one, second first etc.
		temp = pizzas[0]
		for j := 0; j < numberOfPizzas-1; j++ { 
			pizzas[j] = pizzas[j+1]
		}
		pizzas[numberOfPizzas-1] = temp

		// then we check if the slices without last pizza slices are >= max slices
		for k := numberOfPizzas-1; k > 0; k-- {
			tempSum = tempSum - pizzas[k]
			if (tempSum <= slices) {
				//saveTheResult(tempSum, pizzas[:k], possiblePizzasToOrder)
				possiblePizzasToOrder = append(possiblePizzasToOrder, possiblePizzaSet{tempSum, pizzas[:k]})
				break
			}
		}
	}

	//sort the saved possible results and pick the one with highest slices points
	pizzasThatAreActualResult := possiblePizzaSet{possiblePizzasToOrder[0].Slices, possiblePizzasToOrder[0].Pizzas}
	
	for l := 1; l < len(possiblePizzasToOrder); l++  {
		if ( possiblePizzasToOrder[l].Slices > pizzasThatAreActualResult.Slices ) {
			pizzasThatAreActualResult = possiblePizzasToOrder[l]
		}
	}

	// save the output file
	writeResultToFile(pizzasThatAreActualResult)
}

func writeResultToFile(pizzasThatAreActualResult possiblePizzaSet) {
	// save the output file
	outFile, err := os.Create("output.out")
    if err != nil {
        fmt.Println(err)
        outFile.Close()
        return
    }
	defer outFile.Close()
	writer := bufio.NewWriter(outFile)
	
	_, err = writer.WriteString(strconv.Itoa(pizzasThatAreActualResult.Slices) + "\n")
	if err != nil {
		fmt.Println(err)
	  }

	for _, pizza := range pizzasThatAreActualResult.Pizzas {
		_, err = writer.WriteString(strconv.Itoa(pizza) + " ")
		if err != nil {
			fmt.Println(err)
		  }
	}
 
	writer.Flush()

}